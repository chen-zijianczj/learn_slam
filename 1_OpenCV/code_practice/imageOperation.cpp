#include <opencv2/opencv.hpp>
#include <iostream>


using namespace cv;
using namespace std;

int main(int argc, char **argv) {

	Mat src, gray_src;
	src = imread("F:/opencv/car.jpg");
		if (src.empty()) {
			printf_s("could not open image");
			return -1;
		}
	namedWindow("opencv setup demo", CV_WINDOW_AUTOSIZE);
	imshow("opencv setup demo", src);

	cvtColor(src, gray_src,CV_BGR2GRAY);
	namedWindow("output", CV_WINDOW_AUTOSIZE);
	imshow("output", gray_src);
	//单通道
	int height = gray_src.rows;
	int wight = gray_src.cols;

	for (int row = 0; row < height; row++) {
		for (int col = 0; col < wight; col++) {
			int gray = gray_src.at<uchar>(row, col);
			gray_src.at<uchar>(row, col) = 255 - gray;
		}
	}
	namedWindow("output1", CV_WINDOW_AUTOSIZE);
	imshow("output1", gray_src);

	//三通道
	Mat dst;
	dst.create(src.size(),src.type());
	src.copyTo(dst);
	int heightDst = dst.rows;
	int wightDst = dst.cols;
	int nc = src.channels();

	/*for (int row = 0; row < height; row++) {
		for (int col = 0; col < wight; col++) {
			if (nc == 1) {
				int gray = gray_src.at<uchar>(row, col);
				gray_src.at<uchar>(row, col) = 255 - gray;
			}
			else if (nc == 3) {
				int b = dst.at<Vec3b>(row, col)[0];
				int g = dst.at<Vec3b>(row, col)[1];
				int r = dst.at<Vec3b>(row, col)[2];
				dst.at<Vec3b>(row, col)[0] = 255 - b;
				dst.at<Vec3b>(row, col)[1] = 255 - g;
				dst.at<Vec3b>(row, col)[2] = 255 - r;
			}
		}
	}*/

	bitwise_not(src, dst);//按位取反
	namedWindow("output2", CV_WINDOW_AUTOSIZE);
	imshow("output2", dst);



	waitKey(0);

	return 0;
}