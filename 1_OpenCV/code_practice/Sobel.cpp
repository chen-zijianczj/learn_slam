#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char **argv) {
	Mat dst, src, gray_src;
	const char* inputTitle  = "input image";
	const char* outputTitle = "output image";

	src = imread("F:/opencv/Sunny.jpg");	
	if (!src.data) {
		cout<<"could not open image";
		return -1;
	}
	namedWindow(inputTitle,CV_WINDOW_AUTOSIZE);
	imshow(inputTitle, src);

	GaussianBlur(src,dst,Size(3,3),0,0);
	cvtColor(dst, gray_src, CV_BGR2GRAY);
	imshow("gray_src", gray_src);
	
	Mat xgrad, ygrad;
	Sobel(gray_src, xgrad, CV_16S, 1, 0, 3);
	Sobel(gray_src, ygrad, CV_16S, 0, 1, 3);
	convertScaleAbs(xgrad, xgrad);
	convertScaleAbs(ygrad, ygrad);
	imshow("xgrad", xgrad);
	imshow("ygrad", ygrad);

	Mat xygrad = Mat(xgrad.size(),xgrad.type());
	int width = xgrad.cols;
	int height = ygrad.rows;
	for (int row = 0; row < height; row++) {
		for (int col = 0; col < width; col++) {
			int xg = xgrad.at<uchar>(row,col);
			int yg = ygrad.at<uchar>(row, col);
			int xy = xg + yg;
			xygrad.at<uchar>(row, col) = saturate_cast<uchar>(xy);
		}
	}
	//addWeighted(xgrad, 0.5, ygrad, 0.5, 0, xygrad);
	imshow("xygrad", xygrad);

	waitKey(0);
	return 0;
}
