#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

void CallBackDemo(int, void*);

int elementSize = 3;
int maxSize = 21;
Mat dst, src;
const char* inputTitle  = "input image";
const char* outputTitle = "output image";

int main(int argc, char **argv) {

	src = imread("F:/opencv/Sunny.jpg");	
	if (!src.data) {
		cout<<"could not open image";
		return -1;
	}
	namedWindow(inputTitle,CV_WINDOW_AUTOSIZE);
	imshow(inputTitle, src);

	namedWindow(outputTitle, CV_WINDOW_AUTOSIZE);
	createTrackbar("Element Size", outputTitle,&elementSize,maxSize, CallBackDemo);
	CallBackDemo(0,0);

	waitKey(0);
	return 0;
}

void CallBackDemo(int, void*) {
	int s = elementSize * 2 + 1;
	Mat structureElement = getStructuringElement(MORPH_RECT,Size(s,s),Point(-1,-1));
	//dilate(src, dst, structureElement, Point(-1, -1), 1);
	erode(src,dst, structureElement);
	imshow(outputTitle,dst);
	return;
}