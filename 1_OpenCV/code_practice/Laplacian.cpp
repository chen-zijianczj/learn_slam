#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char **argv) {
	Mat dst, src, gray_src;
	const char* inputTitle  = "input image";
	const char* outputTitle = "output image";

	src = imread("F:/opencv/Sunny.jpg");	
	if (!src.data) {
		cout<<"could not open image";
		return -1;
	}
	namedWindow(inputTitle,CV_WINDOW_AUTOSIZE);
	imshow(inputTitle, src);

	Mat edgImage;
	GaussianBlur(src,dst,Size(3,3),0,0);
	cvtColor(dst,gray_src,CV_BGR2GRAY);
	Laplacian(gray_src,edgImage,CV_16S,3);
	convertScaleAbs(edgImage, edgImage);
	namedWindow(outputTitle, CV_WINDOW_AUTOSIZE);
	imshow(outputTitle, edgImage);

	waitKey(0);
	return 0;
}
